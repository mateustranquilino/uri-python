from sys import stdout

def tronco(n):

    stdout.write(" " * (n / 2)) 
    stdout.write("*\n")
    stdout.write(" " * (n / 2 -1))
    stdout.write("***\n")

while True:
    try:

        n = int(raw_input())
        k = n / 2
        for i in xrange(1, n+1, 2):
            stdout.write(" " * k)
            stdout.write("*" * (i-1))
            stdout.write("*\n")
            k -= 1
        
        tronco(n)
        print
    except EOFError:
        break