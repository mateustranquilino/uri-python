def comp(a, b):

	if (mapa[a] > mapa[b]):
		return 1
	elif (mapa[b] > mapa[a]):
		return -1

	return ord(b) - ord(a)

flag = False
while True:
	
	try:
		entrada = raw_input()
		if flag: print
		mapa = {}
		
		for i in entrada:
			if mapa.has_key(i): mapa[i] += 1
			else: mapa[i] = 1
		
		keys = mapa.keys()
		keys.sort(comp)
		
		for e in keys:
			print ord(e), mapa[e]

		flag = True
	except EOFError: break
