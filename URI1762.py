from math import ceil

for i in xrange(int(raw_input())):

    mapa = {}
    for j in xrange(int(raw_input())):
        nome = raw_input()
        peso = float(raw_input())
        mapa[nome] = peso
    
    capacidade = float(raw_input())

    pedido = raw_input()
    qtd = int(raw_input())

    total = 0.0

    while pedido != "-":

        if (mapa.has_key(pedido)):
            total += (mapa[pedido] * qtd)
        
        else:
            print "NAO LISTADO: " + pedido
        
        pedido = raw_input()
        qtd = int(raw_input())

    trenos = ceil( total / capacidade )
    print "Peso total: %.2f kg" % total
    print "Numero de trenos: %d" % trenos
    print
